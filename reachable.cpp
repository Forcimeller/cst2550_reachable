#include <iostream>
#include <fstream>
#include <sstream>
#include "graph.h"

int main(int argc, char* argv[]) {

  if (argc < 3) {
    std::cerr << "USAGE: " << argv[0]
	      << " <GRAPH-FILE> <START-VERTEX>\n";
    exit(1);
  }

  std::string input_file = argv[1];
  unsigned start_vertex = atoi(argv[2]);
  
  std::ifstream infile;
  infile.open(input_file);

  unsigned vertex_qty;
  infile >> vertex_qty;
  
  Digraph graph(vertex_qty);

  std::string line;
  while(std::getline(infile, line)) {
    std::stringstream linestream(line);
    unsigned head, tail;
    linestream >> head;
    while(linestream >> tail)
      graph.add_edge(head, tail);
  }
  infile.close();

  DirectedDFS reachable(graph, start_vertex);

  for (unsigned v = 0; v < graph.V(); ++v)
    if (reachable.is_marked(v))
      std::cout << v << ' ';
  std::cout << std::endl;

  return 0;
}
