#include "graph.h"

/* V: get the number of vertices */
unsigned Digraph::V() const {
  return VERTEX_QTY;
}

/* E: get the number of edges */
unsigned Digraph::E() const {
  return edge_qty;
}

/* add_edge: add an edge from vertex v1, to vertex v2 */
void Digraph::add_edge(unsigned v1, unsigned v2) {
  adjacency_list[v1].push_back(v2);
  ++edge_qty;
}

/* adjacent: get a std::vector of all vertices adjacent 
   to the given vertex */
std::vector<unsigned> Digraph::adjacent(unsigned vertex) {
  return adjacency_list[vertex];
}

/* reverse: get a Digraph with the same vertices 
   as this one, but the direction of all edges 
   are reversed */
Digraph Digraph::reverse() {
  Digraph graph(V());
  for (unsigned v1 = 0; v1 < V(); ++v1)
    for (unsigned v2 : adjacent(v1))
      graph.add_edge(v2, v1);
  return graph;
}

/* constructor: initialise members and perform DFS on the given digraph */
DirectedDFS::DirectedDFS(Digraph graph, unsigned start) {
  marked = std::vector<bool>(graph.V(), false);
  dfs(graph, start);
}

/* dfs: perform depth-first search on the given digraph starting from
   vertex start and marking all visited vertices */
void DirectedDFS::dfs(Digraph graph, unsigned start) {
  marked[start] = true;
  for (unsigned vector : graph.adjacent(start))
    if (!is_marked(vector))
      dfs(graph, vector);
}
  
/* is_marked: check if the given vertex is marked (has been visited during
   DFS) */
bool DirectedDFS::is_marked(unsigned vertex) {
  return marked[vertex];
}
